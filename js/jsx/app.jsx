import React from 'react';
import ReactDOM from 'react-dom';
import Stomp from 'stompjs';

class ChessMain extends React.Component {
    constructor(props){
        super(props);
        this.selectedPlayerChangedHandler = this.selectedPlayerChangedHandler.bind(this);
        this.inviteHandler = this.inviteHandler.bind(this);
        this.messageHandler = this.messageHandler.bind(this);
        this.moveHandler = this.moveHandler.bind(this);
        this.state = {
            selectedplayer: '',
            playersList: [],
            gameStarted: false
        };
        this.socket = new SockJS('/chess-websocket');
        this.stompClient = Stomp.over(this.socket);
        let client = this.stompClient;
        let self = this;
        this.stompClient.connect({}, function(frame){
            self.username = frame.headers['user-name'];
            client.subscribe('/topic/all', function(message){
                self.messageHandler(JSON.parse(message.body));
            });
            client.subscribe('/user/queue/updates', function(message){
                self.messageHandler(JSON.parse(message.body));
            });
            self.stompClient.send('/app/playersList', {priority: 1});
        });
    }

    inviteHandler(e) {
        let player = this.state.selectedplayer.text;
        this.stompClient.send('/app/invite', {priority: 1}, JSON.stringify({'secondUser': player}));
    }

    moveHandler(from, to, gameId) {
        this.stompClient.send('/app/move', {priority: 1}, JSON.stringify({
            'from': from,
            'to': to,
            'gameId': gameId
        }));
    }

    messageHandler(message) {
        switch(message.messageType) {
            case "PLAYER_LIST":
                this.playersChanged(message.players);
                break;
            case "MOVE":
                boardRef.move(message.from + '-' + message.to);
                gameRef.move({
                    from: message.from,
                    to: message.to,
                    promotion: 'q'
                });
                break;
            case "ACCEPT":
                console.log('accept');
                break;
            case "INVITE":
                if(confirm(message.secondUser + ' sent invitation - do you want to play?')) {
                    this.stompClient.send('/app/accept', {priority: 1}, JSON.stringify(
                        {
                            'firstUser': this.username,
                            'secondUser': message.secondUser
                        })
                    );
                }
                break;
            case "START_GAME":
                this.startGame(message);
                break;
            case "CHESS_EVENT":
                break;
        }
    }

    startGame(gameInfo) {
        this.setState({
            firstUser: gameInfo.firstUser,
            secondUser: gameInfo.secondUser,
            gameId: gameInfo.gameId,
            gameStarted: true
        });
    }

    playersChanged(players) {
        this.setState({
            playersList: players
        });
        this.forceUpdate();
    }

    selectedPlayerChangedHandler(e) {
        this.setState({
            selectedplayer: e.target.options[e.target.selectedIndex]
        });
    }

    render() {
        if(!this.state.gameStarted) {
            return (
                <div className="centered">
                    <div className="left">
                        <div>
                            <GameInfoArea onInvite={this.inviteHandler}  selectedplayer={this.state.selectedplayer}/></div>
                        <div><Rules/></div>
                    </div>
                    <div className="left"><Players playersList={this.state.playersList} selectedPlayerChangedHandler={this.selectedPlayerChangedHandler}/></div>
                </div>
            );
        } else
        {
            return (
                <div>
                    <Game   firstUser={this.state.firstUser}
                            secondUser={this.state.secondUser}
                            playAsWhite={this.state.firstUser === this.username}
                            gameId={this.state.gameId}
                            onMove={this.moveHandler}/>
                </div>
            );
        }
    }
}

class Players extends React.Component {
    render() {
        return (
            <div>
                <text fontSize={18}><b>Available players:</b></text><br/>
                <select className="players" id="playersList" multiple="multiple" size="22" onChange={this.props.selectedPlayerChangedHandler}>
                    {
                        this.props.playersList.map(function(player, index){
                            return <option value={index} key={index}>
                                 {player}
                            </option>
                        })
                    }
                </select>
            </div>
        );
    }
}

class GameInfoArea extends React.Component {
    render() {
        return (
            <div>
                <div><img src="img\banner.jpg" alt="Chess TOiK"/></div>
                <div className="playwith">
                    Play with: {this.props.selectedplayer.text} <br/>
                    <button onClick={this.props.onInvite}>Invite</button>
                </div>
            </div>
        );
    }
}

class Rules extends React.Component {
    render() {
        return(
            <textarea defaultValue={getRules()}/>
        );
    }
}

var boardRef = null;
var gameRef = null;

class Game extends React.Component {
    constructor(props) {
        super(props);
        if(gameRef === null) {
            gameRef = new Chess();
            boardRef = null;
        }
    }
    render() {
        return (
            <div>
                <div id="board" className="chessboard">
                    {this.createBoard()}
                </div>
            </div>
        );
    }

    createBoard() {
        if(document.getElementById('board') === null) {
            this.setState({
                dummy: 1
            });
        } else {
            if(boardRef === null) {
                boardRef = ChessBoard('board', {
                    draggable: true,
                    dropOffBoard: 'trash',
                    position: 'start',
                    onDragStart: this.onDragStart,
                    onDrop: this.onDrop,
                    game: gameRef,
                    self: this
                });
            }

            if(!this.props.playAsWhite) {
                boardRef.flip();
            }
        }
    }

    onDragStart(source, piece, position, orientation) {
        if((this.game.turn() === 'w' && !this.self.props.playAsWhite) ||
            (this.game.turn() === 'b' && this.self.props.playAsWhite)) {
            return false;
        }
        if (this.game.game_over() === true ||
            (this.game.turn() === 'w' && piece.search(/^b/) !== -1) ||
            (this.game.turn() === 'b' && piece.search(/^w/) !== -1)) {
            return false;
        }
    };

    onDrop(source, target) {
        // see if the move is legal
        var move = this.game.move({
            from: source,
            to: target,
            promotion: 'q' // NOTE: always promote to a queen for example simplicity
        });

        this.self.props.onMove(source, target, this.self.props.gameId);

        // illegal move
        if (move === null) return 'snapback';
    };
}

ReactDOM.render(<ChessMain />, document.getElementById("app"));

/**
 * Created by unimator on 07.05.2017.
 */

let getPlayers = function(){
    return ['Adam', 'Ewa', 'Edward', 'Alexander', 'Tom'];
};

let getRules = function() {
    //language=HTML
    return "Description" +
        "\nLorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum vehicula mi in lacus semper, eget lobortis dui laoreet. Aliquam euismod nisi a maximus bibendum. Ut dapibus dictum velit pulvinar luctus. Aenean dapibus, ante id aliquam congue, arcu turpis cursus lorem, nec pharetra risus leo suscipit diam. Sed non justo ut eros facilisis vehicula eu et lectus. Nam at massa orci. Aenean sollicitudin commodo quam ut sodales. Sed varius, purus non suscipit fermentum, nisl lacus mattis sem, vitae tristique justo arcu ac ex. Sed nec leo mauris." +
        "\nRules:" +
        "\n" +
        "\n\t - Be nice to other players" +
        "\n\t - No cheating allowed" +
        "\n\t - bla bla bla" +
        "\n\t - ..." +
        "\n";
}
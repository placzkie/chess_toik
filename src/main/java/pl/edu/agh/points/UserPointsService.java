package pl.edu.agh.points;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.users.User;
import pl.edu.agh.users.UserService;

/**
 * Created by leszek on 29.04.17.
 */
@Service
public class UserPointsService {

    @Autowired
    private UserService userService;

    public UserPointsService() {
    }

    public void increaseUserPoints(String username, int points) {
        User user = userService.getUser(username);
        int updatedPoints = user.getPoints() + points;
        user.setPoints(updatedPoints);
        userService.saveUser(user);
    }

    public int getUserPoints(String username) {
        User user = userService.getUser(username);
        return user.getPoints();
    }
}

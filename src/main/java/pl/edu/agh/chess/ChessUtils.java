package pl.edu.agh.chess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;

/**
 * Created by leszek on 30.04.17.
 */
@Component
public class ChessUtils {

    private ScriptEvaluator scriptEvaluator;

    @Autowired
    public ChessUtils(ScriptEvaluator scriptEvaluator) {
        this.scriptEvaluator = scriptEvaluator;
    }

    @PostConstruct
    public void init() throws FileNotFoundException {
        scriptEvaluator.eval(new FileReader("src/main/resources/static/chess.js"));
    }

    public ChessJ createChess() {
        //todo - will it work for several games or each new chess needs to be named differently???
        scriptEvaluator.eval("var chess = new Chess()");
        Chess chess = scriptEvaluator.obtainObject("chess", Chess.class);
        return new ChessJ(chess);
    }

    public static Move createMoveFromMap(Map<String, String> moveMap) {
        if (moveMap == null) {
            return new Move("", "", "", "", "", "", "");
        }
        String color = moveMap.getOrDefault("color", "");
        String from = moveMap.getOrDefault("from", "");
        String to = moveMap.getOrDefault("to", "");
        String flags = moveMap.getOrDefault("flags", "");
        String piece = moveMap.getOrDefault("piece", "");
        String san = moveMap.getOrDefault("san", "");
        String promotion = moveMap.getOrDefault("promotion", "");

        Move move = new Move(color, from, to, flags, piece, san, promotion);

        return move;
    }

}

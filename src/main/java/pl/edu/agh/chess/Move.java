package pl.edu.agh.chess;

/**
 * Created by leszek on 30.04.17.
 */
public class Move {

    private String color;

    private String from;

    private String to;

    private String flags;

    private String piece;

    private String san;

    private String promotion;

    public Move() {
    }

    public Move(String color, String from, String to, String flags, String piece, String san, String promotion) {
        this.color = color;
        this.from = from;
        this.to = to;
        this.flags = flags;
        this.piece = piece;
        this.san = san;
        this.promotion = promotion;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getPiece() {
        return piece;
    }

    public void setPiece(String piece) {
        this.piece = piece;
    }

    public String getSan() {
        return san;
    }

    public void setSan(String san) {
        this.san = san;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    @Override
    public String toString() {
        return "Move{" +
                "color='" + color + '\'' +
                ", from='" + from + '\'' +
                ", to='" + to + '\'' +
                ", flags='" + flags + '\'' +
                ", piece='" + piece + '\'' +
                ", san='" + san + '\'' +
                ", promotion='" + promotion + '\'' +
                '}';
    }
}

package pl.edu.agh.chess;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by leszek on 30.04.17.
 */
public class ChessJ {

    private final Chess chess;


    public ChessJ(Chess chess) {
        this.chess = chess;
    }


    public String fen() {
        return chess.fen();
    }

    public boolean inCheck() {
        return chess.in_check();
    }

    public Map<String, String> move(String san) {
        return chess.move(san);
    }

    public Move move(Move move) {
        Map<String, String> moveMap = chess.move(move);
        return ChessUtils.createMoveFromMap(moveMap);
    }

    public List<Move> moves() {
        List<Map<String, String>> movesMapList = chess.moves_verbose();
        List<Move> movesList = movesMapList.stream()
                                            .map(ChessUtils::createMoveFromMap)
                                            .collect(Collectors.toList());
        return movesList;
    }


    public String turn() {
        return chess.turn();
    }

    public boolean insufficientMaterial() {
        return chess.insufficient_material();
    }

    public boolean load(String fen) {
        return chess.load(fen);
    }

    public boolean inCheckmate() {
        return chess.in_checkmate();
    }

    public boolean gameOver() {
        return chess.game_over();
    }

    public Map<String, String> remove(String square) {
        return chess.remove(square);
    }

    public boolean inStalemate() {
        return chess.in_stalemate();
    }

    public Map<String, Object> validateFen(String fen) {
        return chess.validate_fen(fen);
    }

    public boolean inDraw() {
        return chess.in_draw();
    }

    public boolean inThreefoldRepetition() {
        return chess.in_threefold_repetition();
    }

    public boolean put(Piece piece, String square) {
        return chess.put(piece, square);
    }
}

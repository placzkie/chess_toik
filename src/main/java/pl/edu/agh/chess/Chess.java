package pl.edu.agh.chess;

import java.util.List;
import java.util.Map;

/**
 * Created by leszek on 30.04.17.
 */
public interface Chess {

    String fen();

    boolean game_over();

    boolean in_check();

    boolean in_checkmate();

    boolean in_draw();

    boolean in_stalemate();

    boolean in_threefold_repetition();

    boolean insufficient_material();

    boolean load(String fen);

    Map<String, String> move(String san);

    Map<String, String> move(Move m);

    List<Map<String, String>> moves_verbose();

    boolean put(Piece piece, String square);

    Map<String, String> remove(String square);

    String turn();

    Map<String, Object> validate_fen(String fen);

}

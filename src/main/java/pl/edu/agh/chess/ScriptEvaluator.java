package pl.edu.agh.chess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.Reader;

/**
 * Created by leszek on 30.04.17.
 */
@Component
public class ScriptEvaluator {

    private ScriptEngineManager scriptEngineManager;

    private ScriptEngine scriptEngine;

    private Invocable invocable;


    @Autowired
    public ScriptEvaluator(ScriptEngineManager scriptEngineManager) {
        this.scriptEngineManager = scriptEngineManager;
    }

    @PostConstruct
    public void init() {
        this.scriptEngine = scriptEngineManager.getEngineByName("nashorn");
        invocable = (Invocable) scriptEngine;
    }

    public void eval(Reader reader) {
        try {
            scriptEngine.eval(reader);
        } catch (ScriptException e) {
            throw new RuntimeException("Error occurred while evaluating script", e);
        }
    }

    public void eval(String script) {
        try {
            scriptEngine.eval(script);
        } catch (ScriptException e) {
            throw new RuntimeException("Error occurred while evaluating script", e);
        }
    }

    public <T> T obtainObject(String objectName, Class<T> clazz) {
        Object object = scriptEngine.get(objectName);
        return invocable.getInterface(object, clazz);
    }


    @Configuration
    public static class ScriptEvaluatorConfig {

        @Bean
        public ScriptEngineManager scriptEngineManager() {
            return new ScriptEngineManager();
        }
    }

}

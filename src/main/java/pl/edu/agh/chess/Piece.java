package pl.edu.agh.chess;

/**
 * Created by leszek on 30.04.17.
 */
public class Piece {

    private String type;

    private String color;

    public Piece() {
    }

    public Piece(String type, String color) {
        this.type = type;
        this.color = color;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}

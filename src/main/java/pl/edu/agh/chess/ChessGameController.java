package pl.edu.agh.chess;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pl.edu.agh.games.ActiveGamesService;
import pl.edu.agh.points.UserPointsService;
import pl.edu.agh.websocket.message.*;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.util.List;
import java.util.Optional;

/**
 * Created by leszek on 30.04.17.
 */
@Component
public class ChessGameController {

    @Autowired
    private ActiveGamesService activeGamesService;

    @Autowired
    private UserNotificationService userNotificationService;

    @Autowired
    private UserPointsService userPointsService;


    public ChessGameController() {
    }

    public void move(String gameId, Move move, String mover) {
        Optional<ChessJ> chessOptional = activeGamesService.getGameForId(gameId);
        ChessJ chess = chessOptional.orElseThrow(() -> new RuntimeException("No game for id:" + gameId));

        if (isMoveLegal(move, chess)) {
            makeMove(move, chess, gameId);
            controlGameStates(chess, gameId, mover);
        }
    }

    private boolean isMoveLegal(Move move, ChessJ chess) {
        List<Move> legalMoves = chess.moves();

        Optional<Move> anyMove = legalMoves.stream()
                                            .filter(m -> m.getFrom().equals(move.getFrom()) &&
                                                    m.getTo().equals(move.getTo()))
                                            .findAny();
        return anyMove.isPresent();
    }

    private void makeMove(Move move, ChessJ chess, String gameId) {
        Move movePerformed = chess.move(move);
        MoveMessage moveMessage =
                            new MoveMessage(movePerformed.getFrom(), movePerformed.getTo(), move.getPromotion(), gameId);
        notifyPlayers(gameId, moveMessage);
    }

    private void controlGameStates(ChessJ chess, String gameId, String mover) {
        if (chess.gameOver()) {
            ChessEventMessage chessEventMessage = new ChessEventMessage();
            if (chess.inCheckmate()) {
                chessEventMessage.setEventType(ChessEventType.CHECKMATE);
            } else if (chess.inStalemate()) {
                chessEventMessage.setEventType(ChessEventType.STALEMATE);
            } else if (chess.inDraw()) {
                chessEventMessage.setEventType(ChessEventType.DRAW);
            } else if (chess.inThreefoldRepetition()) {
                chessEventMessage.setEventType(ChessEventType.THREEFOLD_REPETITION);
            } else if (chess.insufficientMaterial()) {
                chessEventMessage.setEventType(ChessEventType.INSUFFICIENT_MATERIAL);
            }

            updateUsersPoints(gameId, mover, chessEventMessage.getEventType());
            notifyPlayers(gameId, chessEventMessage);
        } else if (chess.inCheck()) {
            ChessEventMessage chessEventMessage = new ChessEventMessage(ChessEventType.CHECK);
            notifyPlayers(gameId, chessEventMessage);
        }
    }

    private void updateUsersPoints(String gameId, String mover, ChessEventType chessEventType) {
        Optional<List<String>> gamesUsersOptional = activeGamesService.getGamesUsers(gameId);
        List<String> gamesUsers = gamesUsersOptional.orElseThrow(
                                                            () -> new RuntimeException("No users for id: " + gameId));
        switch (chessEventType) {
            case CHECKMATE:
                userPointsService.increaseUserPoints(mover, 100);
                break;
            case DRAW:
                gamesUsers.stream().forEach(user -> userPointsService.increaseUserPoints(user, 50));
                break;
            case INSUFFICIENT_MATERIAL:
                gamesUsers.stream().forEach(user -> userPointsService.increaseUserPoints(user, 50));
                break;
            case STALEMATE:
                gamesUsers.stream().forEach(user -> userPointsService.increaseUserPoints(user, 50));
                break;
            case THREEFOLD_REPETITION:
                gamesUsers.stream().forEach(user -> userPointsService.increaseUserPoints(user, 50));
                break;
            default:
                break;
        }

    }

    private void notifyPlayers(String gameId, Message message) {
        Optional<List<String>> gamesUsersOptional = activeGamesService.getGamesUsers(gameId);
        List<String> gamesUsers = gamesUsersOptional.orElseThrow(
                                                            () -> new RuntimeException("No users for id: " + gameId));

        gamesUsers.stream().forEach(user -> userNotificationService.notifySingleUser(user, message));
    }
}

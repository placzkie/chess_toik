package pl.edu.agh.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Created by leszek on 28.04.17.
 */
@Service
@Profile("mysql")
public class DatabaseUserService implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public boolean shouldAuthenticate(String user, String password) {
        Optional<User> userOptional = Optional.ofNullable(userRepository.findByName(user));
        return userOptional.map(User::getPassword).filter(p -> p.equals(password)).isPresent();
    }

    @Override
    public void addUser(String name, String password) {
        User user = new User(name, password);
        userRepository.save(user);
    }

    @Override
    public User getUser(String username) {
        return userRepository.findByName(username);
    }

    @Override
    public void saveUser(User user) {
        userRepository.save(user);
    }
}

package pl.edu.agh.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by leszek on 29.04.17.
 */
@RestController
public class UserRestController {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/rest/user", method = RequestMethod.POST)
    public void addUser(@RequestBody UserCredentials userCredentials) {
        String name = userCredentials.getName();
        String password = userCredentials.getPassword();

        userService.addUser(name, password);
    }
}

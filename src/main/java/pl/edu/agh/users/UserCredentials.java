package pl.edu.agh.users;

/**
 * Created by leszek on 29.04.17.
 */
public class UserCredentials {

    private String name;

    private String password;

    public UserCredentials() {
    }

    public UserCredentials(String name, String password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

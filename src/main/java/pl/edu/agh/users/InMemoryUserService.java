package pl.edu.agh.users;

import com.google.common.collect.Maps;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Created by leszek on 28.04.17.
 */
@Service
@Profile("in_memory")
public class InMemoryUserService implements UserService {

    private final Map<String, String> userPasswords;

    private final Map<String, Integer> userPoints;


    public InMemoryUserService() {
        this.userPasswords = prepareUserRepository();
        this.userPoints = prepareUserPoints();
    }


    @Override
    public boolean shouldAuthenticate(String user, String password) {
        return userPasswords.containsKey(user) && password.equals(userPasswords.get(user));
    }

    @Override
    public void addUser(String user, String password) {
        userPasswords.put(user, password);
        userPoints.put(user, 0);
    }

    @Override
    public User getUser(String username) {
        String password = userPasswords.get(username);
        Integer points = userPoints.get(username);
        return new User(username, password, points);
    }

    @Override
    public void saveUser(User user) {
        userPasswords.put(user.getName(), user.getPassword());
        userPoints.put(user.getName(), user.getPoints());
    }

    private static Map<String, String> prepareUserRepository() {
        final Map<String, String> users = Maps.newHashMap();
        users.put("a", "a");
        users.put("b", "b");
        users.put("c", "c");

        return users;
    }

    private static Map<String, Integer> prepareUserPoints() {
        final Map<String, Integer> userPoints = Maps.newHashMap();
        userPoints.put("a", 0);
        userPoints.put("b", 100);
        userPoints.put("c", 200);

        return userPoints;
    }
}

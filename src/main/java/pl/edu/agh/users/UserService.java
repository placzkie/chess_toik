package pl.edu.agh.users;

/**
 * Created by leszek on 28.04.17.
 */
public interface UserService {

    boolean shouldAuthenticate(String user, String password);

    void addUser(String user, String password);

    User getUser(String username);

    void saveUser(User user);
}

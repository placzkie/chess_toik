package pl.edu.agh.users;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import pl.edu.agh.websocket.message.Message;
import pl.edu.agh.websocket.message.PlayerListMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by leszek on 29.04.17.
 */
@Service
public class ActiveUsersService {

    private UserNotificationService userNotificationService;

    private Map<String, Boolean> activeUsers;

    @Autowired
    public ActiveUsersService(@Lazy UserNotificationService userNotificationService) {
        this.userNotificationService = userNotificationService;
        this.activeUsers = new ConcurrentHashMap<>();
    }

    public void addUser(String user) {
        activeUsers.put(user, false);
        notifyUsersOfNewList();
    }

    public void removeUser(String user) {
        activeUsers.remove(user);
        notifyUsersOfNewList();
    }

    public void markUserAsPlaying(String user) {
        activeUsers.put(user, true);
        notifyUsersOfNewList();
    }

    public void markUserAsFree(String user) {
        activeUsers.put(user, false);
        notifyUsersOfNewList();
    }

    public Set<String> getFreeUsers() {
        Set<String> freeUsers = activeUsers.entrySet().stream()
                                                        .filter(entry -> !entry.getValue())
                                                        .map(Map.Entry::getKey)
                                                        .collect(Collectors.toSet());
        return freeUsers;
    }

    private void notifyUsersOfNewList() {
        Set<String> freeUsers = getFreeUsers();
        Message message = new PlayerListMessage(Lists.newArrayList(freeUsers));
        userNotificationService.notifyAllUsers(message);
    }
}

package pl.edu.agh.users;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Leszek Placzkiewicz on 2017-05-20.
 */
@Entity
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    private String password;

    private int points;

    protected User() {
    }

    public User(String name, String password) {
        this(name, password, 0);
    }

    public User(String name, String password, int points) {
        this.name = name;
        this.password = password;
        this.points = points;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return String.format("User[id=%d, name='%s', password='%s']", id, name, password);
    }
}

package pl.edu.agh.users;

import org.springframework.data.repository.CrudRepository;

/**
 * Created by Leszek Placzkiewicz on 2017-05-20.
 */
public interface UserRepository extends CrudRepository<User, Long> {

    User findByName(String name);
}

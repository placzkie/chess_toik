package pl.edu.agh.websocket.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import pl.edu.agh.websocket.message.Message;

/**
 * Created by leszek on 29.04.17.
 */
@Service
public class UserNotificationService {

    private static final String ALL_USERS_PATH = "/topic/all";

    private static final String SINGLE_USER_PATH = "/queue/updates";


    private SimpMessagingTemplate messagingTemplate;


    @Autowired
    public UserNotificationService(SimpMessagingTemplate messagingTemplate) {
        this.messagingTemplate = messagingTemplate;
    }


    public void notifyAllUsers(Message message) {
        messagingTemplate.convertAndSend(ALL_USERS_PATH, message);
    }

    public void notifySingleUser(String user, Message message) {
        messagingTemplate.convertAndSendToUser(user, SINGLE_USER_PATH, message);
    }

}

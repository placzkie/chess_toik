package pl.edu.agh.websocket.message;

/**
 * Created by Leszek Placzkiewicz on 2017-05-21.
 */
public class UserPointsMessage extends Message {

    private String user;

    private int points;

    public UserPointsMessage() {
        this(null, 0);
    }

    public UserPointsMessage(String user, int points) {
        super(MessageType.USER_POINTS);
        this.user = user;
        this.points = points;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}

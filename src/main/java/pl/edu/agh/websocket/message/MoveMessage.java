package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 28.04.17.
 */
public class MoveMessage extends Message {

    private String from;

    private String to;

    private String promotion;

    private String gameId;


    public MoveMessage() {
        this(null, null, null, null);
    }

    public MoveMessage(String from, String to, String promotion, String gameId) {
        super(MessageType.MOVE);
        this.from = from;
        this.to = to;
        this.promotion = promotion;
        this.gameId = gameId;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPromotion() {
        return promotion;
    }

    public void setPromotion(String promotion) {
        this.promotion = promotion;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}

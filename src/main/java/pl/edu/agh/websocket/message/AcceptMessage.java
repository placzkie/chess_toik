package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 29.04.17.
 */
public class AcceptMessage extends Message {

    private String firstUser;

    private String secondUser;

    public AcceptMessage() {
        this(null, null);
    }

    public AcceptMessage(String firstUser, String secondUser) {
        super(MessageType.ACCEPT);
        this.firstUser = firstUser;
        this.secondUser = secondUser;
    }

    public String getFirstUser() {
        return firstUser;
    }

    public void setFirstUser(String firstUser) {
        this.firstUser = firstUser;
    }

    public String getSecondUser() {
        return secondUser;
    }

    public void setSecondUser(String secondUser) {
        this.secondUser = secondUser;
    }
}

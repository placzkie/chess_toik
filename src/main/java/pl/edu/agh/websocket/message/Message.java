package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 29.04.17.
 */
public abstract class Message {

    private MessageType messageType;

    public Message() {
    }

    public Message(MessageType messageType) {
        this.messageType = messageType;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }
}
package pl.edu.agh.websocket.message;

import java.util.List;

/**
 * Created by leszek on 29.04.17.
 */
public class PlayerListMessage extends Message {

    private List<String> players;

    public PlayerListMessage() {
        this(null);
    }

    public PlayerListMessage(List<String> players) {
        super(MessageType.PLAYER_LIST);
        this.players = players;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }
}

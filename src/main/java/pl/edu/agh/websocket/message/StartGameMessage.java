package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 29.04.17.
 */
public class StartGameMessage extends Message {

    private String firstUser;

    private String secondUser;

    private String gameId;


    public StartGameMessage() {
        this(null, null, null);
    }

    public StartGameMessage(String firstUser, String secondUser, String gameId) {
        super(MessageType.START_GAME);
        this.firstUser = firstUser;
        this.secondUser = secondUser;
        this.gameId = gameId;

    }

    public String getFirstUser() {
        return firstUser;
    }

    public void setFirstUser(String firstUser) {
        this.firstUser = firstUser;
    }

    public String getSecondUser() {
        return secondUser;
    }

    public void setSecondUser(String secondUser) {
        this.secondUser = secondUser;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }
}

package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 01.05.17.
 */
public enum ChessEventType {
    CHECK,
    CHECKMATE,
    STALEMATE,
    DRAW,
    THREEFOLD_REPETITION,
    INSUFFICIENT_MATERIAL

}

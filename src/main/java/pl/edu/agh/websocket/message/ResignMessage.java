package pl.edu.agh.websocket.message;

/**
 * Created by Leszek Placzkiewicz on 2017-05-21.
 */
public class ResignMessage extends Message{

    private String gameId;

    private String resignee;

    public ResignMessage() {
        this(null, null);
    }

    public ResignMessage(String gameId, String resignee) {
        super(MessageType.RESIGN);
        this.gameId = gameId;
        this.resignee = resignee;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getResignee() {
        return resignee;
    }

    public void setResignee(String resignee) {
        this.resignee = resignee;
    }
}

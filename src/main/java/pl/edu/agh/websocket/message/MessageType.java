package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 30.04.17.
 */
public enum MessageType {
    MOVE,
    ACCEPT,
    INVITE,
    PLAYER_LIST,
    START_GAME,
    CHESS_EVENT,
    RESIGN,
    USER_POINTS,
}

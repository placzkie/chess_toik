package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 29.04.17.
 */
public class InviteMessage extends Message {

    private String secondUser;

    public InviteMessage() {
        this(null);
    }

    public InviteMessage(String secondUser) {
        super(MessageType.INVITE);
        this.secondUser = secondUser;
    }

    public String getSecondUser() {
        return secondUser;
    }

    public void setSecondUser(String secondUser) {
        this.secondUser = secondUser;
    }
}

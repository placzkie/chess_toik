package pl.edu.agh.websocket.message;

/**
 * Created by leszek on 01.05.17.
 */
public class ChessEventMessage extends Message {

    private ChessEventType eventType;

    public ChessEventMessage() {
        this(null);
    }

    public ChessEventMessage(ChessEventType eventType) {
        super(MessageType.CHESS_EVENT);
        this.eventType = eventType;
    }

    public ChessEventType getEventType() {
        return eventType;
    }

    public void setEventType(ChessEventType eventType) {
        this.eventType = eventType;
    }
}

package pl.edu.agh.websocket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptorAdapter;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.web.socket.config.annotation.AbstractWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import pl.edu.agh.games.ActiveGamesService;
import pl.edu.agh.points.UserPointsService;
import pl.edu.agh.users.ActiveUsersService;
import pl.edu.agh.websocket.message.ResignMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.security.Principal;
import java.util.Optional;

/**
 * Created by leszek on 28.04.17.
 */
@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractWebSocketMessageBrokerConfigurer {

    @Autowired
    private ActiveUsersService activeUsersService;

    @Autowired
    private ActiveGamesService activeGamesService;

    @Autowired
    @Lazy
    private UserNotificationService userNotificationService;

    @Autowired
    private UserPointsService userPointsService;


    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic", "/queue");
        config.setApplicationDestinationPrefixes("/app");
    }

    @Override
    public void registerStompEndpoints(StompEndpointRegistry registry) {
        registry.addEndpoint("/chess-websocket").withSockJS();
    }

    @Override
    public void configureClientInboundChannel(ChannelRegistration registration) {
        registration.setInterceptors(new ChannelInterceptorAdapter() {

            @Override
            public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
                super.afterSendCompletion(message, channel, sent, ex);

                StompHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

                if (StompCommand.CONNECT.equals(accessor.getCommand())) {
                    Principal principal = accessor.getUser();
                    activeUsersService.addUser(principal.getName());
                } else if (StompCommand.DISCONNECT.equals(accessor.getCommand())) {
                    Principal principal = accessor.getUser();
                    activeUsersService.removeUser(principal.getName());

                    Optional<String> gameIdOptional = activeGamesService.getUserGameId(principal.getName());
                    if (gameIdOptional.isPresent()) {
                        String gameId = gameIdOptional.get();
                        String opponent = activeGamesService.getUserOpponent(principal.getName());

                        userNotificationService.notifySingleUser(opponent, new ResignMessage(gameId, principal.getName()));
                        userPointsService.increaseUserPoints(opponent, 100);
                        activeGamesService.removeGame(gameId);
                    }

                }
            }

        });
    }
}
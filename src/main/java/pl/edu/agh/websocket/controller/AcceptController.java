package pl.edu.agh.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import pl.edu.agh.games.ActiveGamesService;
import pl.edu.agh.users.ActiveUsersService;
import pl.edu.agh.websocket.message.AcceptMessage;
import pl.edu.agh.websocket.message.StartGameMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

/**
 * Created by leszek on 29.04.17.
 */
@Controller
public class AcceptController {

    private UserNotificationService userNotificationService;

    private ActiveUsersService activeUsersService;

    private ActiveGamesService activeGamesService;

    @Autowired
    public AcceptController(UserNotificationService userNotificationService, ActiveUsersService activeUsersService,
                            ActiveGamesService activeGamesService) {
        this.userNotificationService = userNotificationService;
        this.activeUsersService = activeUsersService;
        this.activeGamesService = activeGamesService;
    }


    @MessageMapping("/accept")
    public void accept(AcceptMessage message) throws Exception {
        String firstUser = message.getFirstUser();
        String secondUser = message.getSecondUser();

        activeUsersService.markUserAsPlaying(firstUser);
        activeUsersService.markUserAsPlaying(secondUser);

        String gameId = activeGamesService.createGame(firstUser, secondUser);

        userNotificationService.notifySingleUser(firstUser, new StartGameMessage(firstUser, secondUser, gameId));
        userNotificationService.notifySingleUser(secondUser, new StartGameMessage(firstUser, secondUser, gameId));
    }
}

package pl.edu.agh.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import pl.edu.agh.games.ActiveGamesService;
import pl.edu.agh.points.UserPointsService;
import pl.edu.agh.users.ActiveUsersService;
import pl.edu.agh.websocket.message.ResignMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

/**
 * Created by Leszek Placzkiewicz on 2017-05-21.
 */
public class ResignController {

    private UserNotificationService userNotificationService;

    private ActiveUsersService activeUsersService;

    private ActiveGamesService activeGamesService;

    private UserPointsService userPointsService;

    @Autowired
    public ResignController(UserNotificationService userNotificationService, ActiveUsersService activeUsersService,
                            ActiveGamesService activeGamesService, UserPointsService userPointsService) {
        this.userNotificationService = userNotificationService;
        this.activeUsersService = activeUsersService;
        this.activeGamesService = activeGamesService;
        this.userPointsService = userPointsService;
    }

    @MessageMapping("/resign")
    public void resign(ResignMessage message) throws Exception {
        String gameId = message.getGameId();
        String resignee = message.getResignee();

        activeGamesService.removeGame(gameId);
        activeUsersService.markUserAsFree(resignee);
        String opponent = activeGamesService.getUserOpponent(resignee);
        userNotificationService.notifySingleUser(opponent, message);
        userPointsService.increaseUserPoints(opponent, 100);
    }
}

package pl.edu.agh.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import pl.edu.agh.points.UserPointsService;
import pl.edu.agh.websocket.message.UserPointsMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

/**
 * Created by Leszek Placzkiewicz on 2017-05-21.
 */
public class UserPointsController {

    private UserNotificationService userNotificationService;

    private UserPointsService userPointsService;

    @Autowired
    public UserPointsController(UserNotificationService userNotificationService, UserPointsService userPointsService) {
        this.userNotificationService = userNotificationService;
        this.userPointsService = userPointsService;
    }


    @MessageMapping("/userPoints")
    public void userPoints(UserPointsMessage message) throws Exception {
        String user = message.getUser();
        int userPoints = userPointsService.getUserPoints(user);
        userNotificationService.notifySingleUser(user, new UserPointsMessage(user, userPoints));
    }

}

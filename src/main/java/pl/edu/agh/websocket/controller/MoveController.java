package pl.edu.agh.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import pl.edu.agh.chess.ChessGameController;
import pl.edu.agh.chess.Move;
import pl.edu.agh.games.ActiveGamesService;
import pl.edu.agh.websocket.message.MoveMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.security.Principal;

/**
 * Created by leszek on 28.04.17.
 */
@Controller
public class MoveController {

    private ActiveGamesService activeGamesService;

    private ChessGameController chessGameController;

    private UserNotificationService userNotificationService;


    @Autowired
    public MoveController(UserNotificationService userNotificationService,
                          ActiveGamesService activeGamesService,
                          ChessGameController chessGameController) {
        this.activeGamesService = activeGamesService;
        this.userNotificationService = userNotificationService;
        this.chessGameController = chessGameController;
    }

    @MessageMapping("/move")
    public void move(MoveMessage message, Principal principal) throws Exception {
        Move move = new Move();
        move.setFrom(message.getFrom());
        move.setTo(message.getTo());
        move.setPromotion(message.getPromotion());
        chessGameController.move(message.getGameId(), move, principal.getName());
    }
}

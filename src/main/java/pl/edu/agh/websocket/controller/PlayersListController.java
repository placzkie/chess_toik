package pl.edu.agh.websocket.controller;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import pl.edu.agh.users.ActiveUsersService;
import pl.edu.agh.websocket.message.Message;
import pl.edu.agh.websocket.message.PlayerListMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.util.Set;

/**
 * Created by Leszek Placzkiewicz on 2017-05-20.
 */
@Controller
public class PlayersListController {

    private UserNotificationService userNotificationService;

    private ActiveUsersService activeUsersService;

    @Autowired
    public PlayersListController(UserNotificationService userNotificationService, ActiveUsersService activeUsersService) {
        this.userNotificationService = userNotificationService;
        this.activeUsersService = activeUsersService;
    }


    @MessageMapping("/playersList")
    public void playersList() throws Exception {
        sendPlayersListToUsers();
    }

    private void sendPlayersListToUsers() {
        Set<String> freeUsers = activeUsersService.getFreeUsers();
        Message message = new PlayerListMessage(Lists.newArrayList(freeUsers));
        userNotificationService.notifyAllUsers(message);
    }
}

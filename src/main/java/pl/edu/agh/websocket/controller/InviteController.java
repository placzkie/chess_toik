package pl.edu.agh.websocket.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import pl.edu.agh.websocket.message.InviteMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.security.Principal;

/**
 * Created by leszek on 29.04.17.
 */
@Controller
public class InviteController {

    private UserNotificationService userNotificationService;

    @Autowired
    public InviteController(UserNotificationService userNotificationService) {
        this.userNotificationService = userNotificationService;
    }

    @MessageMapping("/invite")
    public void invite(InviteMessage message, Principal principal) throws Exception {
        String secondUser = message.getSecondUser();
        userNotificationService.notifySingleUser(secondUser, new InviteMessage(principal.getName()));
    }
}

package pl.edu.agh.games;

import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.agh.chess.ChessJ;
import pl.edu.agh.chess.ChessUtils;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by leszek on 29.04.17.
 */
@Service
public class ActiveGamesService {

    @Autowired
    private ChessUtils chessUtils;

    private final Map<UUID, List<String>> gamesUsers;

    private final Map<String, UUID> usersGames;

    private final Map<UUID, ChessJ> games;


    public ActiveGamesService() {
        this.gamesUsers = new ConcurrentHashMap<>();
        this.usersGames = new ConcurrentHashMap<>();
        this.games = new ConcurrentHashMap<>();
    }


    public String createGame(String firstPlayer, String secondPlayer) {
        final UUID gameUuid = UUID.randomUUID();
        gamesUsers.put(gameUuid, ImmutableList.of(firstPlayer, secondPlayer));
        usersGames.put(firstPlayer, gameUuid);
        usersGames.put(secondPlayer, gameUuid);
        ChessJ chess = chessUtils.createChess();
        games.put(gameUuid, chess);
        return gameUuid.toString();
    }

    public void removeGame(String gameId) {
        final UUID uuid = UUID.fromString(gameId);
        List<String> users = gamesUsers.get(uuid);
        usersGames.remove(users.get(0));
        usersGames.remove(users.get(1));
        gamesUsers.remove(uuid);
        games.remove(uuid);
    }

    public String getUserOpponent(String user) {
        UUID uuid = usersGames.get(user);
        List<String> users = gamesUsers.get(uuid);
        Optional<String> opponentOptional = users.stream().filter(u -> !u.equals(user)).findFirst();
        return opponentOptional.orElse("");
    }

    public Optional<ChessJ> getGameForId(String gameId) {
        UUID uuid = UUID.fromString(gameId);
        return Optional.ofNullable(games.get(uuid));
    }

    public Optional<List<String>> getGamesUsers(String gameId) {
        UUID uuid = UUID.fromString(gameId);
        return Optional.ofNullable(gamesUsers.get(uuid));
    }

    public Optional<String> getUserGameId(String user) {
        UUID uuid = usersGames.get(user);
        return Optional.ofNullable(uuid).map(UUID::toString);
    }

}

package pl.edu.agh.chess;

import com.google.common.collect.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.edu.agh.games.ActiveGamesService;
import pl.edu.agh.points.UserPointsService;
import pl.edu.agh.websocket.message.ChessEventMessage;
import pl.edu.agh.websocket.message.ChessEventType;
import pl.edu.agh.websocket.message.Message;
import pl.edu.agh.websocket.message.MoveMessage;
import pl.edu.agh.websocket.service.UserNotificationService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * Created by Leszek Placzkiewicz on 2017-06-07.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ChessGameControllerTest {

    @MockBean
    private ActiveGamesService activeGamesService;

    @MockBean
    private UserNotificationService userNotificationService;

    @MockBean
    private UserPointsService userPointsService;

    @MockBean
    private ChessJ chess;


    @Autowired
    private ChessGameController chessGameController;

    @Test
    public void moveWithoutGameOver() throws Exception {
        //given
        initChess(false, false, false);
        given(activeGamesService.getGameForId(any())).willReturn(Optional.of(chess));
        given(activeGamesService.getGamesUsers(any())).willReturn(Optional.of(Arrays.asList("user1", "user2")));

        //when
        String gameId = "12345";
        Move move = new Move("w", "d4", "c5", "", "p", "c5", "");
        String mover = "user1";
        chessGameController.move(gameId, move, mover);

        //then
        verifyZeroInteractions(userPointsService);


        verify(activeGamesService).getGameForId(gameId);
        verify(activeGamesService).getGamesUsers(gameId);
        verifyNoMoreInteractions(activeGamesService);


        verify(chess).move(move);
        verify(chess).moves();
        verify(chess).gameOver();
        verify(chess).inCheck();
        verifyNoMoreInteractions(chess);


        ArgumentCaptor<String> usersCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<MoveMessage> messagesCaptor = ArgumentCaptor.forClass(MoveMessage.class);
        verify(userNotificationService, times(2)).notifySingleUser(usersCaptor.capture(), messagesCaptor.capture());

        List<String> usersCaptured = usersCaptor.getAllValues();
        assertEquals("user1", usersCaptured.get(0));
        assertEquals("user2", usersCaptured.get(1));

        List<MoveMessage> messagesCaptured = messagesCaptor.getAllValues();
        messagesCaptured.stream().forEach(this::validateMoveMessage);
        verifyNoMoreInteractions(userNotificationService);
    }

    @Test
    public void moveWithGameOverDraw() throws Exception {
        //given
        initChess(true, false, true);
        given(activeGamesService.getGameForId(any())).willReturn(Optional.of(chess));
        given(activeGamesService.getGamesUsers(any())).willReturn(Optional.of(Arrays.asList("user1", "user2")));

        //when
        String gameId = "12345";
        Move move = new Move("w", "d4", "c5", "", "p", "c5", "");
        String mover = "user1";
        chessGameController.move(gameId, move, mover);

        //then
        verify(activeGamesService).getGameForId(gameId);
        verify(activeGamesService, times(3)).getGamesUsers(gameId);
        verifyNoMoreInteractions(activeGamesService);


        verify(chess).move(move);
        verify(chess).moves();
        verify(chess).gameOver();
        verify(chess).inCheckmate();
        verify(chess).inStalemate();
        verify(chess).inDraw();
        verifyNoMoreInteractions(chess);


        ArgumentCaptor<String> usersCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Message> messagesCaptor = ArgumentCaptor.forClass(Message.class);
        verify(userNotificationService, times(4)).notifySingleUser(usersCaptor.capture(), messagesCaptor.capture());
        verifyNoMoreInteractions(userNotificationService);

        List<String> usersCaptured = usersCaptor.getAllValues();
        assertEquals("user1", usersCaptured.get(0));
        assertEquals("user2", usersCaptured.get(1));
        assertEquals("user1", usersCaptured.get(2));
        assertEquals("user2", usersCaptured.get(3));

        List<Message> messagesCaptured = messagesCaptor.getAllValues();
        messagesCaptured.subList(0, 2).stream().map(msg -> (MoveMessage) msg).forEach(this::validateMoveMessage);
        messagesCaptured.subList(2, 4).stream().map(msg -> (ChessEventMessage) msg)
                                                .forEach(cem -> assertEquals(ChessEventType.DRAW, cem.getEventType()));


        usersCaptor = ArgumentCaptor.forClass(String.class);
        ArgumentCaptor<Integer> pointsCaptor = ArgumentCaptor.forClass(Integer.class);
        verify(userPointsService, times(2)).increaseUserPoints(usersCaptor.capture(), pointsCaptor.capture());

        usersCaptured = usersCaptor.getAllValues();
        assertEquals("user1", usersCaptured.get(0));
        assertEquals("user2", usersCaptured.get(1));

        List<Integer> pointsCaptured = pointsCaptor.getAllValues();
        assertEquals(Integer.valueOf(50), pointsCaptured.get(0));
        assertEquals(Integer.valueOf(50), pointsCaptured.get(1));
    }

    private void initChess(boolean gameOver, boolean inCheck, boolean inDraw) {
        List<Move> legalMoves = prepareLegalMoves();

        Move movePerformed = new Move();
        movePerformed.setFrom("d4");
        movePerformed.setTo("c5");

        given(chess.moves()).willReturn(legalMoves);
        given(chess.move(any(Move.class))).willReturn(movePerformed);
        given(chess.gameOver()).willReturn(gameOver);
        given(chess.inCheck()).willReturn(inCheck);
        given(chess.inDraw()).willReturn(inDraw);
    }

    private List<Move> prepareLegalMoves() {
        List<Move> legalMoves = Lists.newArrayList();

        Move move = new Move();
        move.setFrom("a2");
        move.setTo("a3");
        legalMoves.add(move);

        move = new Move();
        move.setFrom("d4");
        move.setTo("c5");
        legalMoves.add(move);

        return legalMoves;
    }

    private void validateMoveMessage(MoveMessage moveMessage) {
        assertEquals("d4", moveMessage.getFrom());
        assertEquals("c5", moveMessage.getTo());
        assertEquals("", moveMessage.getPromotion());
        assertEquals("12345", moveMessage.getGameId());
    }

}
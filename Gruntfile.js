/**
 * Created by unimator on 04.05.2017.
 */

'use strict';

module.exports = function(grunt) {

    let buildJs = ['sync'];
    let buildJsx = ['babel', 'browserify'].concat(buildJs);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        watch: {
            options: {
              livereload: true
            },
            js: {
                files: ['js/**/*.js'],
                tasks: buildJs,
            },
            jsx: {
                files: ['js/jsx/**/*.jsx'],
                tasks: buildJsx,
            },
            html: {
                files: ['src/main/resources/static/**/*.html', 'src/main/resources/static/**/*.css'],
                tasks: ['sync'],
            },
            img: {
                files: ['src/main/resources/static/img/*'],
                tasks: ['sync'],
            }
        },

        browserify: {
            dist: {
                files: {
                    'dist/js/module.js' : ['dist/js-compiled/**/*.js']
                }
            }
        },

        sync: {
            copyWebFilesToWARExploded: {
                files: [
                    {
                        cwd: 'dist/js',
                        src: '**/*.js',
                        dest: 'target/classes/static/js'
                    },
                    {
                        cwd: 'js',
                        src: '**/*.js',
                        dest: 'target/classes/static'
                    },
                    {
                        cwd: 'src/main/resources/static/img',
                        src: '**/*',
                        dest: 'target/classes/static/img'
                    },
                    {
                        cwd: 'src/main/resources/static',
                        src: ['**/*.html', '**/*.css'],
                        dest: 'target/classes/static'
                    }
                ]
            }
        },

        uglify: {
            min: {
                files: {
                    'dist/js/app.min.js' : ['js/**/*.js', 'dist/js/jsx-compiled/module.js']
                }
            }
        },

        babel: {
            options: {
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            jsx: {
                files: [{
                    expand: true,
                    cwd: 'js/jsx',
                    src: ['**/*.jsx'],
                    dest: 'dist/js-compiled/',
                    ext: '.js'
                }]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-sync");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-babel");
    grunt.loadNpmTasks("grunt-browserify");
    grunt.registerTask('default', buildJsx);
};
